Anshika Singh
CSE 262

Question 1. 

Number 1 (λp.pz) λq.w λw.wqzp
Answer: ((λp.pz) λq.w λw.wqzp)
	((λp.pz) (λq.w λw.wqzp))
	((λp.pz) (λq.(w λw.wqzp)))
	((λp.pz) (λq.(w (λw.wqzp))))
	((λp.pz) (λq.(w (λw.(wqzp))))) <- final
2. λp.pq λp.qp
Answer: (λp.pq λp.qp)
	(λp.(pq) λp.qp)
	(λp.(pq) (λp.qp))
	(λp.(pq) (λp.(qp))) <- Final



Question 2.

1. λs.s z λq.s q
A s is bound to the first λ. The q is bound
to the second λ. The variable z is free.


2. (λs. s z) λq. w λw. w q z s
s is bound to the first λ. The q is bound to the second λ. The first w' is free, and 
the second w is bound to the third λ. The variable z' is free.


3. (λs.s) (λq.qs)
Answer: The variable s is bound to the first λ. The variable q is bound to the second lambda. s' is free.


4. λz. (((λs.sq) (λq.qz)) λz. (z z))
Answer:  z is bound to the first λ. s is bound to the second λ. q is
free when it is grouped with the second lambda but is bound to the third lambda. Both z's at the end are bound to the last lambda.


Question 3.

1. (λz.z) (λq.q q) (λs.s a)
   (λq.q q) (λs.s a)
   (λs.s a) (λs.s a)
   (λs.s a) a
   a a'


2. (λz.z) (λz.z z) (λz.z q)
   (λz.z z) (λz.z q)
   (λz.z q) (λz.z q)
   (λz.z q) q
   q q
    

3. (λs.λq.s q q) (λa.a) b
   (λs.(λq.s q q)) (λa.a) b
   (λs.(λq.(s q q))) (λa.a) b
   (λq.λa.a q q) b
   (λa.a) b b
   b b

4. (λs.λq.s q q) (λq.q) q
   (λq.(λq.q) q q) q
   (λq.(λa.a) q q) q
   (λa.a) q q
   q q

5. ((λs.s s) (λq.q)) (λq.q)
   (λq.q) (λq.q) (λq.q)
  (λr.r)
  (λq.q)
   λq.q 
 


Question 4.



1. Truth table for the OR operator:
p(T) q(T) pvq(T)
p(T) q(F) pvq(T)
p(F) q(T) pvq(T)
p(F) q(F) pvq(F)
2. Church encoding for OR = (λp.λq.p p q)
This the reduction of the church encoding:
(λp.λq.p p q)(λa.λb.a)(λa.λb.b)
(λa.λb.a)(λa.λb.a)(λa.λb.b)
(λa.λb.a)
Therefore we can derive true from this Church encoding.

Proof: 
1.λa.λb.a means true and λa.λb.b means false. so when 
2. OR means p or q
you derive the or, you get true.



Question 5. 
Here are some of the current operators:
AND = (λ.xy (y (x FALSE TRUE) FALSE))
OR = (λ.xy (x TRUE (y TRUE FALSE)))

We can derive these operators because of this assumption:
FALSE = λ.xy y
TRUE = λ.xy x

This gives us the conclusion:
NOT = (λa. (a FALSE TRUE))
This is ver similar to a if statement because if the output is true, and if a is true, the output is false. 
